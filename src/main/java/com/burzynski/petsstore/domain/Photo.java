package com.burzynski.petsstore.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

@Embeddable
public class Photo {
    @Column(name = "PET_ID")
    private Long petId;
    @Column(name = "PHOTO_URL")
    private String photoUrl;
}
