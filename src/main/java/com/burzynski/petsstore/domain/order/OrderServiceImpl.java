package com.burzynski.petsstore.domain.order;

import com.burzynski.petsstore.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.HashMap;
import java.util.Map;

import static com.burzynski.petsstore.domain.order.OrderStatus.*;

@Service
class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Map<String, Integer> getStatusToQuantityOrderMap() {
        Map<String, Integer> statusToQuantityOrderMap = new HashMap<>();

        statusToQuantityOrderMap.put(APPROVED.name(), sumQuantitiesByStatus(APPROVED));
        statusToQuantityOrderMap.put(DELIVERED.name(), sumQuantitiesByStatus(DELIVERED));
        statusToQuantityOrderMap.put(PLACED.name(), sumQuantitiesByStatus(PLACED));
        return statusToQuantityOrderMap;
    }

    @Override
    public Order placeOrder(Order body) {
        return orderRepository.save(body);
    }

    @Override
    public Order findOrderById(@Min(value = 1) @Max(value = 10) Long orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Order not found"));
    }

    @Override
    public void deleteOrderById(@Min(value = 1) Long orderId) {
        orderRepository.deleteById(orderId);
    }

    private Integer sumQuantitiesByStatus(OrderStatus orderStatus) {
        return orderRepository.findAllByStatus(orderStatus).stream().mapToInt(order -> order.getQuantity().intValue()).sum();
    }
}
