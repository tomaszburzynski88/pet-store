package com.burzynski.petsstore.domain.order;

import com.burzynski.petsstore.domain.pet.Pet;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ORDERS")
@Data
@Builder
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PET_ID")
    private Pet pet;
    @Column(name = "QUANTITY")
    private Integer quantity;
    @Column(name = "SHIP_DATE")
    private Date shipDate;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private OrderStatus status;
    @Column(name = "complete")
    private boolean complete = false;
}
