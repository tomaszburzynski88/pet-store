package com.burzynski.petsstore.domain.order;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Map;

public interface OrderService {

    Map<String, Integer> getStatusToQuantityOrderMap();

    Order placeOrder(Order body);

    Order findOrderById(@Min(value = 1) @Max(value = 10) Long orderId);

    void deleteOrderById(@Min(value = 1) Long orderId);
}
