package com.burzynski.petsstore.domain.order;

public enum OrderStatus {
    PLACED, APPROVED, DELIVERED
}
