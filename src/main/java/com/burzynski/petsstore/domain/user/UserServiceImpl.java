package com.burzynski.petsstore.domain.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(User body) {
        userRepository.save(body);
    }

    @Override
    public void createUsersWithArrayInput(User... body) {
        userRepository.saveAll(Arrays.asList(body));
    }

    @Override
    public void createUsersWithListInput(List<User> body) {
        userRepository.saveAll(body);
    }
}
