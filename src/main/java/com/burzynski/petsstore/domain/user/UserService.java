package com.burzynski.petsstore.domain.user;

import java.util.List;

public interface UserService {
    void createUser(User body);

    void createUsersWithArrayInput(User... body);

    void createUsersWithListInput(List<User> body);
}
