package com.burzynski.petsstore.domain.tag;

import com.burzynski.petsstore.domain.pet.Pet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "TAG")
@Data
@Builder
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PET_ID")
    private Pet pet;
}
