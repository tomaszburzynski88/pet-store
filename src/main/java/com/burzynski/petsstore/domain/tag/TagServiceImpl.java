package com.burzynski.petsstore.domain.tag;

import com.burzynski.petsstore.rest.exception.InvalidTagException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> findTagsByNameIn(String... names) {
        return tagRepository.findTagsByNameIn(names).orElseThrow(() -> new InvalidTagException("Invalid tag value"));
    }

    @Override
    public Tag saveTag(Tag tag) {
        return tagRepository.save(tag);
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }
}
