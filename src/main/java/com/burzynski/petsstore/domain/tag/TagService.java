package com.burzynski.petsstore.domain.tag;

import java.util.List;

public interface TagService {

    List<Tag> findTagsByNameIn(String... names);

    Tag saveTag(Tag tag);

    List<Tag> findAll();

}
