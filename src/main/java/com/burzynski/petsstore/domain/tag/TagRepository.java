package com.burzynski.petsstore.domain.tag;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Optional<List<Tag>> findTagsByNameIn(String... name);
}
