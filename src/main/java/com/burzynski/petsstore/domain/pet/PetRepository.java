package com.burzynski.petsstore.domain.pet;

import com.burzynski.petsstore.domain.tag.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PetRepository extends JpaRepository<Pet, Long> {

    Optional<List<Pet>> findAllByStatus(PetStatus status);

    Optional<List<Pet>> findAllByTagsIn(List<Tag> tags);
}
