package com.burzynski.petsstore.domain.pet;

import com.burzynski.petsstore.rest.exception.InvalidStatusException;

public enum PetStatus {
    AVAILABLE, PENDING, SOLD;

    public static PetStatus getEnum(String name){
        for(PetStatus value : values()){
            if(value.name().equals(name)){
                return value;
            }
        }
        throw new InvalidStatusException("Status not found");
    }
}
