package com.burzynski.petsstore.domain.pet;

import java.util.List;

public interface PetStore {

    void addPetToStore(Pet pet);

    Pet updatePet(Pet pet);

    List<Pet> findByStatus(String petStatus);

    List<Pet> findByTags(String[] tagNames);

    Pet findById(Long id);

    Pet updatePetWithForm(Long petId, String name, String status);

    void deletePet(Long petId);
}
