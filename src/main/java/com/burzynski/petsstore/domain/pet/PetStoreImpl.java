package com.burzynski.petsstore.domain.pet;

import com.burzynski.petsstore.domain.tag.Tag;
import com.burzynski.petsstore.domain.tag.TagService;
import com.burzynski.petsstore.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
class PetStoreImpl implements PetStore{

    private PetRepository petRepository;
    private TagService tagService;

    @Autowired
    public PetStoreImpl(PetRepository petRepository, TagService tagService){
        this.petRepository = petRepository;
        this.tagService = tagService;
    }

    @Override
    public void addPetToStore(Pet pet) {
        petRepository.save(pet);
    }

    @Override
    public Pet updatePet(Pet update) {
        Pet pet = findById(update.getId());
        return petRepository.save(setPetNewValues(pet, update));
    }

    @Override
    public List<Pet> findByStatus(String petStatusString) {
        return petRepository.findAllByStatus(PetStatus.getEnum(petStatusString)).orElseThrow(() -> new NotFoundException());
    }

    @Override
    public List<Pet> findByTags(String[] tagNames) {
        List<Tag> tags = tagService.findTagsByNameIn(tagNames);
        return petRepository.findAllByTagsIn(tags).orElseThrow(()-> new NotFoundException());
    }

    @Override
    public Pet findById(Long id) {
        return petRepository.findById(id).orElseThrow(() -> new NotFoundException("Pet not found"));
    }

    @Override
    public Pet updatePetWithForm(Long petId, String name, String status) {
        Pet pet = findById(petId);
        pet.setName(name);
        pet.setStatus(PetStatus.getEnum(status));
        return updatePet(pet);
    }

    @Override
    public void deletePet(Long petId) {
        Pet pet = findById(petId);
        petRepository.delete(pet);
    }

    private Pet setPetNewValues(Pet pet, Pet newPet){
        pet.setId(newPet.getId());
        pet.setCategoryList(newPet.getCategoryList());
        pet.setName(newPet.getName());
        pet.setPhotoUrls(newPet.getPhotoUrls());
        pet.setStatus(newPet.getStatus());
        pet.setTags(newPet.getTags());
        return pet;
    }
}
