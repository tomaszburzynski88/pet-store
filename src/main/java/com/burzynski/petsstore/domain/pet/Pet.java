package com.burzynski.petsstore.domain.pet;

import com.burzynski.petsstore.domain.Category;
import com.burzynski.petsstore.domain.tag.Tag;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PET")
@Data
@Builder
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @ManyToMany
    @JoinTable(
            name="PET_TO_CATEGORY",
            joinColumns = @JoinColumn(name = "PET_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "CATEGORY_ID", referencedColumnName = "ID")
    )
    private List<Category> categoryList;
    @Column(name = "NAME", nullable = false)
    private String name;
    @ElementCollection
    @CollectionTable(name = "PHOTO", joinColumns = @JoinColumn(name = "PET_ID"))
    @Column(name = "PHOTO_URLS")
    private List<String> photoUrls = new ArrayList<>();
    @OneToMany()
    @JoinColumn(name="PET_ID", referencedColumnName="ID")
    private List<Tag> tags;
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private PetStatus status;

}