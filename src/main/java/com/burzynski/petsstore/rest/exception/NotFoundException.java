package com.burzynski.petsstore.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {
    public NotFoundException(){
        super();
    }

    public NotFoundException(String message, Throwable throwable){
        super(message, throwable);
    }

    public NotFoundException(String message){
        super(message);
    }

    public NotFoundException(Throwable throwable){
        super(throwable);
    }
}
