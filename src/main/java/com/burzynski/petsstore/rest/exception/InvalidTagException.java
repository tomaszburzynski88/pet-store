package com.burzynski.petsstore.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidTagException extends RuntimeException {

    public InvalidTagException(){
        super();
    }

    public InvalidTagException(String message, Throwable throwable){
        super(message, throwable);
    }

    public InvalidTagException(String message){
        super(message);
    }

    public InvalidTagException(Throwable throwable){
        super(throwable);
    }
}
