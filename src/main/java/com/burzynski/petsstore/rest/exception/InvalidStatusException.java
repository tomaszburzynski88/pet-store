package com.burzynski.petsstore.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidStatusException extends RuntimeException{
    public InvalidStatusException(){
        super();
    }

    public InvalidStatusException(String message, Throwable throwable){
        super(message, throwable);
    }

    public InvalidStatusException(String message){
        super(message);
    }

    public InvalidStatusException(Throwable throwable){
        super(throwable);
    }
}
