package com.burzynski.petsstore.rest;

import com.burzynski.petsstore.domain.pet.Pet;
import com.burzynski.petsstore.domain.pet.PetStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

import static org.springframework.http.MediaType.*;

@RestController
@RequestMapping(value = "/pet")
public class PetStoreController {

    private PetStore petStore;

    @Autowired
    public PetStoreController(PetStore petStore){
        this.petStore = petStore;
    }

    @PostMapping(consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void addPet(@RequestBody Pet body){
        petStore.addPetToStore(body);
    }

    @PutMapping(consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void updatePet(@RequestBody Pet body){
        petStore.updatePet(body);
    }

    @Deprecated
    @GetMapping(value = "/findByStatus", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public List<Pet> findPetsByStatus(@RequestParam(name = "status", defaultValue = "available") String status){
        return petStore.findByStatus(status);
    }

    @Deprecated
    @GetMapping(value = "/findByTags", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public List<Pet> findPetsByTags(@RequestParam(name = "tags") String[] tagNames){
        return petStore.findByTags(tagNames);
    }

    @GetMapping(value = "/{petId}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public Pet getPetById(@RequestHeader(name = "api_key", required = false) String apiKey,@PathVariable(name = "petId") Long petId ){
        return petStore.findById(petId);
    }

    @PostMapping(value = "/{petId}", consumes = APPLICATION_FORM_URLENCODED_VALUE, produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public Pet updatePetWithForm(@PathVariable(name = "petId") Long petId, @RequestParam(name = "name") String name, @RequestParam(name = "status", required = false) String status){
        return petStore.updatePetWithForm(petId, name, status);
    }

    @DeleteMapping(value = "/{petId}", produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public void deletePet(@RequestHeader(name = "api_key", required = false) String apiKey, @PathVariable(name = "petId") Long petId){
        petStore.deletePet(petId);
    }

    @PostMapping(value = "/{petId}/uploadImage", consumes = MULTIPART_FORM_DATA_VALUE)
    public Pet uploadFile(@PathVariable(name = "petId") Long petId, @RequestParam(name = "additionalMetadata", required = false) String additionalMetadata, @RequestParam(name = "file", required = false) File file){
        return null;
    }

}
