package com.burzynski.petsstore.rest;

import com.burzynski.petsstore.domain.user.User;
import com.burzynski.petsstore.domain.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void createUser(@RequestBody User body) {
        userService.createUser(body);

    }

    @PostMapping(value = "/createWithArray", produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public void createUsersWithArrayInput(@RequestBody User[] body) {
        userService.createUsersWithArrayInput(body);
    }

    @PostMapping(value = "/createWithList", produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public void createUsersWithListInput(@RequestBody List<User> body) {
        userService.createUsersWithListInput(body);
    }

    @GetMapping(value = "/login", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void loginUser(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password) {
    }

    @GetMapping(value = "/logout", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void logoutUSer() {
    }

    @GetMapping(value = "/{username}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public User getUserByName(@PathVariable(name = "username") String username) {
        return null;
    }

    @PutMapping(value = "/{username}", produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public User updateUser(@PathVariable(name = "username") String username, @RequestBody User user) {
        return null;
    }

    @DeleteMapping(value = "/{username}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public void deleteUser(@PathVariable(name = "username") String username) {
    }
}
