package com.burzynski.petsstore.rest;

import com.burzynski.petsstore.domain.order.Order;
import com.burzynski.petsstore.domain.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;


@RestController
@RequestMapping(value = "/store")
public class StoreController {


    private OrderService orderService;

    @Autowired
    public StoreController(OrderService orderService){
        this.orderService = orderService;
    }

    @Transactional
    @GetMapping(value="/inventory", produces = APPLICATION_JSON_VALUE)
    public Map<String, Integer> getInventory(){
        return orderService.getStatusToQuantityOrderMap();
    }

    @PostMapping(value = "/order")
    public Order placeOrder(@RequestBody Order body){
        return orderService.placeOrder(body);
    }

    @GetMapping(value = "/order/{orderId}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public Order getOrderById(@PathVariable(name = "orderId") @Min(value = 1) @Max(value = 10) Long orderId){
        return orderService.findOrderById(orderId);
    }

    @DeleteMapping(value = "/order/{orderId}", produces = {APPLICATION_XML_VALUE, APPLICATION_JSON_VALUE})
    public void deleteOrder(@PathVariable(name = "orderId") @Min(value = 1) Long orderId){
        orderService.deleteOrderById(orderId);
    }
}
