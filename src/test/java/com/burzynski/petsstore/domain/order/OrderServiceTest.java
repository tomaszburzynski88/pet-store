package com.burzynski.petsstore.domain.order;

import com.burzynski.petsstore.domain.pet.Pet;
import com.burzynski.petsstore.domain.pet.PetRepository;
import com.burzynski.petsstore.domain.pet.PetStatus;
import com.burzynski.petsstore.rest.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"liquibase.change-log=classpath:liquibase/db-changelog-master.xml", "liquibase.drop-first=true"})
@SpringBootTest
@Transactional
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private PetRepository petRepository;

    @Before
    public void setUp() {
        Pet pet = Pet.builder()
                .status(PetStatus.AVAILABLE)
                .name("Pet1")
                .photoUrls(Arrays.asList("http://www.onet.pl"))
                .build();

        pet = petRepository.save(pet);


        Order order = Order.builder()
                .complete(false)
                .quantity(1)
                .shipDate(new Date())
                .status(OrderStatus.APPROVED)
                .pet(pet)
                .build();

        orderRepository.save(order);

        Order order2 = Order.builder()
                .complete(false)
                .quantity(3)
                .shipDate(new Date())
                .status(OrderStatus.APPROVED)
                .pet(pet)
                .build();

        orderRepository.save(order2);
    }

    @Test
    public void testGetStatusToQuantityOrderMap() {
        Assert.assertEquals(4, orderService.getStatusToQuantityOrderMap().get(OrderStatus.APPROVED.name()).intValue());
    }

    @Test
    public void testPlaceOrder() {
        OrderService orderService = mock(OrderServiceImpl.class);
        Order order = mock(Order.class);
        when(orderService.placeOrder(order)).thenReturn(order);
    }

    @Test
    public void testFindOrderById() {
        OrderService orderService = mock(OrderServiceImpl.class);
        Order order = mock(Order.class);
        when(orderService.findOrderById(1L)).thenReturn(order);
    }

    @Test(expected = NotFoundException.class)
    public void testFindOrderByIdNotFoundException() {
        when(orderService.findOrderById(-1L)).thenThrow(new NotFoundException());
    }
}
