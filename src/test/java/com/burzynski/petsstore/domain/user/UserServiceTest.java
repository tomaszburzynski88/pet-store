package com.burzynski.petsstore.domain.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"liquibase.change-log=classpath:liquibase/db-changelog-master.xml", "liquibase.drop-first=true"})
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testCreateUser() {
        UserService userService = mock(UserServiceImpl.class);
        User user = mock(User.class);
        userService.createUser(user);
        verify(userService).createUser(user);
    }

    @Test
    public void testCreateUsersWithArrayInput() {
        UserService userService = mock(UserServiceImpl.class);
        User user = mock(User.class);
        userService.createUsersWithArrayInput(user);
        verify(userService).createUsersWithArrayInput(user);
    }

    @Test
    public void testCreateUsersWithListInput() {
        UserService userService = mock(UserServiceImpl.class);
        User user = mock(User.class);
        userService.createUsersWithListInput(Arrays.asList(user));
        verify(userService).createUsersWithListInput(Arrays.asList(user));
    }
}
