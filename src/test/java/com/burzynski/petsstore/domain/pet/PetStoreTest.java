package com.burzynski.petsstore.domain.pet;

import com.burzynski.petsstore.rest.exception.InvalidTagException;
import com.burzynski.petsstore.rest.exception.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"liquibase.change-log=classpath:liquibase/db-changelog-master.xml", "liquibase.drop-first=true"})
@SpringBootTest
public class PetStoreTest {

    @Autowired
    private PetStore petStore;

    @Before
    public void setUpPetStore(){
        Pet pet = Pet.builder()
                .name("Super Pet")
                .photoUrls(Arrays.asList("https://78.media.tumblr.com/11e9ba28a5b0e313a5958413e6cb66b3/tumblr_mqpsdmMSDW1scitl9o1_500.gif"))
                .status(PetStatus.AVAILABLE)
                .build();

        petStore.addPetToStore(pet);
    }


    @Test
    public void testAddPetToStore(){
        Pet pet = Pet.builder()
                .name("Super Pet")
                .photoUrls(Arrays.asList("https://78.media.tumblr.com/11e9ba28a5b0e313a5958413e6cb66b3/tumblr_mqpsdmMSDW1scitl9o1_500.gif"))
                .status(PetStatus.AVAILABLE)
                .build();
        petStore.addPetToStore(pet);
        assertNotNull(petStore.findById(1L));
    }

    @Test
    public void testUpdatePet(){
        PetStore petStore = mock(PetStoreImpl.class);
        Pet pet = mock(Pet.class);
        when(petStore.updatePet(pet)).thenReturn(pet);
    }

    @Test(expected = NotFoundException.class)
    public void testUpdatePetThrowsNotFoundException(){
        Pet pet = Pet.builder()
                .id(-1L).build();
        when(petStore.updatePet(pet)).thenThrow(new NotFoundException());
    }

    @Test
    public void testFindByStatus(){
        PetStore petStore = mock(PetStoreImpl.class);
        List<Pet> petList = mock(List.class);

        when(petStore.findByStatus(PetStatus.AVAILABLE.name())).thenReturn(petList);
    }


    @Test
    public void testFindByTags(){
        PetStore petStore = mock(PetStoreImpl.class);
        String[] tag = {"tag1"};
        List<Pet> petList = mock(List.class);

        when(petStore.findByTags(tag)).thenReturn(petList);
    }

    @Test(expected = InvalidTagException.class)
    public void testFindByTagsThrowsInvalidTag(){
        String[] tag = {"tag1"};
        List<Pet> petList = mock(List.class);
        when(petStore.findByTags(tag)).thenReturn(petList);
    }
}
