package com.burzynski.petsstore.controller;

import com.burzynski.petsstore.domain.pet.Pet;
import com.burzynski.petsstore.domain.pet.PetStatus;
import com.burzynski.petsstore.domain.tag.Tag;
import com.burzynski.petsstore.domain.tag.TagService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;


@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"liquibase.change-log=classpath:liquibase/db-changelog-master.xml", "liquibase.drop-first=true"})
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PetStoreControllerTest {

    @LocalServerPort
    private int serverPort;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TagService tagService;

    @Before
    public void setUp() {
        RestAssured.baseURI = "http://localhost/";
        RestAssured.port = serverPort;
    }

    @Test
    public void testAddPet() {
        Pet pet = Pet.builder()
                .name("Super Pet")
                .status(PetStatus.PENDING)
                .photoUrls(asList("http://cdn2-www.dogtime.com/assets/uploads/2011/01/file_23020_dachshund-dog-breed.jpg")).build();

        Assert.assertEquals(200, given().contentType("application/json").body(pet).when().post("/pet").andReturn().getStatusCode());
    }

    @Test
    public void testUpdatePet() throws IOException {
        Pet pet = Pet.builder()
                .name("Super Pet")
                .status(PetStatus.PENDING)
                .photoUrls(asList("http://cdn2-www.dogtime.com/assets/uploads/2011/01/file_23020_dachshund-dog-breed.jpg")).build();

        Assert.assertEquals(200, given().contentType("application/json").body(pet).when().post("/pet").andReturn().getStatusCode());
        pet.setId(1L);
        pet.setCategoryList(new ArrayList<>());
        pet.setTags(new ArrayList<>());
        List<Pet> savedPets = objectMapper.readValue(given().contentType("application/json").queryParam("status", PetStatus.PENDING.name()).when().get("/pet/findByStatus").andReturn().getBody().print(), new TypeReference<List<Pet>>() {
        });

        Assert.assertEquals(objectMapper.writeValueAsString(pet), objectMapper.writeValueAsString(savedPets.get(0)));
        Pet petToUpdate = savedPets.get(0);
        petToUpdate.setStatus(PetStatus.AVAILABLE);
        Assert.assertEquals(200, given().contentType("application/json").body(petToUpdate).when().put("/pet").andReturn().getStatusCode());
    }

    @Test
    public void testFindPetsByStatus() throws IOException {
        Pet pet = Pet.builder()
                .name("Super Pet")
                .status(PetStatus.PENDING)
                .photoUrls(asList("http://cdn2-www.dogtime.com/assets/uploads/2011/01/file_23020_dachshund-dog-breed.jpg"))
                .build();
        Assert.assertEquals(200, given().contentType("application/json").body(pet).when().post("/pet").andReturn().getStatusCode());
        Assert.assertEquals(1, ((List) objectMapper.readValue(given().contentType("application/json").queryParam("status", PetStatus.PENDING.name()).when().get("/pet/findByStatus").andReturn().getBody().print(), new TypeReference<List<Pet>>() {
        })).size());
    }

    @Test
    public void testFindPetsByTags() throws IOException {
        Tag tag = Tag.builder().name("Tag1").build();
        tag = tagService.saveTag(tag);

        Pet pet = Pet.builder()
                .name("Super Pet")
                .photoUrls(asList("http://cdn2-www.dogtime.com/assets/uploads/2011/01/file_23020_dachshund-dog-breed.jpg"))
                .status(PetStatus.PENDING)
                .tags(asList(tag))
                .build();
        Assert.assertEquals(200, given().contentType("application/json").body(pet).when().post("/pet").andReturn().getStatusCode());
        List<Pet> petList = objectMapper.readValue(given().contentType("application/json").queryParam("tags", "Tag1").when().get("/pet/findByTags").andReturn().getBody().print(), new TypeReference<List<Pet>>() {
        });
        Assert.assertEquals(1, petList.size());
    }

    @Test
    public void testGetPetById() throws IOException {
        Pet pet = Pet.builder()
                .name("Super Pet")
                .status(PetStatus.PENDING)
                .photoUrls(asList("http://cdn2-www.dogtime.com/assets/uploads/2011/01/file_23020_dachshund-dog-breed.jpg"))
                .build();
        Assert.assertEquals(200, given().contentType("application/json").body(pet).when().post("/pet").andReturn().getStatusCode());
        List<Pet> petList = objectMapper.readValue(given().contentType("application/json").queryParam("status", PetStatus.PENDING.name()).when().get("/pet/findByStatus").andReturn().getBody().print(), new TypeReference<List<Pet>>() {
        });
        Assert.assertEquals(1, petList.size());
        Assert.assertEquals(petList.get(0), objectMapper.readValue(given().contentType("application/json").pathParam("petId", petList.get(0).getId()).when().get("/pet/{petId}").andReturn().getBody().print(), Pet.class));

    }
}
